<?php /* Template Name: Home */

if (!defined('ABSPATH')) exit;

get_header(); ?>

<main id="main" role="main" tabindex="-1">
    <?php
    the_content();
    ?>
</main>

<?php get_footer(); ?>