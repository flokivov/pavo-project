<?php

namespace Inc;

/**
 * gut functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package gut
 */

if (!defined('ABSPATH')) exit;
define('PREVIEWBLOCKIMGDIR', get_stylesheet_directory_uri() . '/img/blocks-previews/');

if (file_exists(__DIR__ . '/vendor/autoload.php')) {
    require_once __DIR__ . '/vendor/autoload.php';
}



require_once __DIR__ . '/inc/settings/settings.php';
require_once __DIR__ . '/inc/acf/acf.php';
require_once __DIR__ . '/inc/admin/admin.php';
require_once __DIR__ . '/inc/utils/utils.php';
require_once __DIR__ . '/inc/Blocks/blocks-require.php';

use StoutLogic\AcfBuilder\FieldsBuilder;



add_action('init', function () {
    if (!function_exists('acf_add_local_field_group') || !function_exists('collect')) {
        return;
    }

    collect(glob(__DIR__ . '/inc/Fields/*.php'))->map(function ($field) {
        return require_once($field);
    })->map(function ($field) {
        if ($field instanceof FieldsBuilder) {
            acf_add_local_field_group($field->build());
        }
    });
});



