<?php

/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package tech-task
 */

get_header();

?>

<main id="main" role="main" tabindex="-1">

    <section class="s-404">
        <div class="container">
            404
        </div>
    </section>

</main>

<?php
get_footer(); ?>