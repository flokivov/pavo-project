const SmoothScroll = () => {
    document.querySelectorAll('a[href*="#"]:not([href="#"])').forEach(anchor => {
        anchor.addEventListener('click', function (e) {
            e.preventDefault();

            document.querySelector(this.getAttribute('href')).scrollIntoView({
                behavior: 'smooth',
            });
        });
    });

    $('.nav-link').on('click', function () {
        $('.navbar-collapse').collapse('hide');
    });
}

export default SmoothScroll;
SmoothScroll();
