wp.domReady(() => {
    wp.blocks.registerBlockStyle('core/list', {
        name: 'colored-list',
        label: 'Colored list',
    });

    wp.blocks.registerBlockVariation(
        'core/list',
        [
            {
                name: 'separator',
                title: 'List With Separator',
                attributes: {
                    className: 'is-underlined',
                },
            },
        ]
    );
});