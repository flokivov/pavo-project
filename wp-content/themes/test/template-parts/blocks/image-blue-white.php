<?php
$title = get_field('image_blue_white_title');
$subTitle = get_field('image_blue_white_subtitle');
$description = get_field('image_blue_white_description');
$link = get_field('image_blue_white_link');
$image = get_field('image_blue_white_image');
$select = get_field('image_blue_white_select');
$quote = get_field('image_blue_white_quote');
$numbers = get_field('image_blue_white_nth');
$id = get_field('image_blue_white_id');
?>

<?php if ($select == 'Left') :
?>
    <section class="image-blue-white" <?php if($id) : ?>id="<?php echo $id; ?>"<?php endif; ?> >
        <div class="row w-100 mx-0">
            <div class="col-lg-5 p-0" style="background-color:#fff;<?php if ($image) : ?>background-image:url('<?php echo $image; ?>'); <?php endif; ?>">
            </div>
            <div class="col-lg-7 p-0">
                <div class="">
                    <div class="h2-div">
                        <?php if ($title) : ?> <h2><?php if($numbers): ?> <span><?php echo $numbers; ?></span> <?php endif; ?><?php echo $title; ?></h2><?php endif; ?>
                    </div>
                    <div class="section-bg-blue container-right-section">
                        <?php if ($subTitle) : ?><h5><?php echo $subTitle ?></h5> <?php endif; ?>
                        <?php if ($description) : ?> 
                            <?php if($quote): ?>
                            <div class="wysiwyg wysiwyg-quote"><img src="<?php echo get_template_directory_uri().'/img/global/quote.png' ?>"><?php echo $description; ?></div>
                            <?php else : ?>
                                <div class="wysiwyg"><?php echo $description; ?></div>
                            <?php endif; ?>
                        <?php endif; ?>
                        <?php if ($link) : ?>
                            <div class="div-a">
                                <a href="<?php echo esc_url($link['url']); ?>" class="btn-orange"><?php echo esc_html($link['title']); ?></a>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

        </div>
    </section>

<?php else : ?>
    <section class="image-blue-white" <?php if($id) : ?>id="<?php echo $id; ?>"<?php endif; ?> >
        <div class="row w-100 mx-0">
            <div class="col-lg-7 p-0">
                <div class="">
                    <div class="h2-div">
                        <?php if ($title) : ?> <h2><span><?php echo $numbers; ?></span><?php echo $title; ?></h2><?php endif; ?>
                    </div>
                    <div class="section-bg-blue container-right-section">
                        <?php if ($subTitle) : ?><h5><?php echo $subTitle ?></h5> <?php endif; ?>
                        <?php if ($description) : ?> <div class="wysiwyg"><?php echo $description; ?></div><?php endif; ?>
                            <?php if ($link) : ?>
                            <div class="div-a">
                                <a href="<?php echo esc_url($link['url']); ?>" class="btn-orange"><?php echo esc_html($link['title']); ?></a>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 p-0" style="background-color:#fff;<?php if ($image) : ?>background-image:url('<?php echo $image; ?>'); <?php endif; ?>">
            </div>

        </div>
    </section>
<?php endif; ?>