<?php
$firstTitle = get_field('first_sections_title');
$firstLink = get_field('first_sections_link');
$firstDescription = get_field('first_sections_description');
$firstImage = get_field('first_sections_image');
$secondTitle = get_field('second_sections_title');
$secondLinkFirst = get_field('second_sections_first_link');
$secondLinkSecond = get_field('second_sections_second_link');
$secondDescription = get_field('second_sections_description');
$secondImage = get_field('second_sections_image');
$firstId = get_field('first_sections_id');
$secondId = get_field('second_sections_id');
?>


<div class="position-relative first-second-block">
    <section class="first-section" <?php if($firstId) : ?>id="<?php echo $firstId; ?>"<?php endif; ?> >
        <div class="row w-100 mx-0">
            <div class="col-lg-7 section-bg-blue p-0">
                <div class="container d-flex justify-content-center flex-column container-section container-right-section">
                    <?php if ($firstTitle) : ?> <h2><?php echo $firstTitle; ?></h2> <?php endif; ?>
                    <?php if ($firstDescription) : ?><div class="wysiwyg"><?php echo $firstDescription; ?></div> <?php endif; ?>
                    <?php if ($firstLink) : ?>
                        <div class="div-a">
                            <a href="<?php echo esc_url($firstLink['url']); ?>" class="btn-orange"><?php echo esc_html($firstLink['title']); ?></a>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-lg-5 p-0" style="background-color:#2C537E;<?php if ($firstImage) : ?>background-image:url('<?php echo $firstImage; ?>'); <?php endif; ?>">

            </div>
        </div>
    </section>
    <section class="first-section" <?php if($secondId) : ?>id="<?php echo $secondId; ?>"<?php endif; ?>>
        <div class="row w-100 mx-0">
            <div class="col-lg-7 section-bg-white p-0">
                <div class="container-section container d-flex justify-content-center flex-column container-right-section">
                    <?php if ($secondTitle) : ?> <h2><?php echo $secondTitle; ?></h2> <?php endif; ?>
                    <?php if ($secondDescription) : ?><div class="wysiwyg"><?php echo $secondDescription; ?></div> <?php endif; ?>
                    <?php if ($secondLinkFirst || $secondLinkSecond) : ?>
                        <div class="d-flex div-a">
                            <?php if ($secondLinkFirst) : ?><a href="<?php echo esc_url($secondLinkFirst['url']); ?>" class="btn-orange"><?php echo esc_html($secondLinkFirst['title']); ?></a><?php endif; ?>
                            <?php if ($secondLinkSecond) : ?><a href="<?php echo esc_url($secondLinkSecond['url']); ?>" class="btn-orange"><?php echo esc_html($secondLinkSecond['title']); ?></a><?php endif; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-lg-5 p-0" style="background-color:#fff;<?php if ($secondImage) : ?>background-image:url('<?php echo $secondImage; ?>'); <?php endif; ?>">
            </div>
        </div>
    </section>
    <?php echo get_first_section_icon(); ?>

</div>