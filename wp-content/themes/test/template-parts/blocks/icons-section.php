<?php
$repeater = get_field('icons_section_repeater');
$id = get_field('icons_section_id');
if($repeater):
?>

<section class="container four-column" <?php if($id) : ?>id="<?php echo $id; ?>"<?php endif; ?> >
        <div class="row">
            <?php foreach($repeater as $row) :
               $icon = $row['icons_section_select']; 
               $title = $row['icons_section_text'];
                ?>
            <div class="col-lg-3 d-flex">
                <?php echo get_icon($icon); ?> <span><?php echo $title; ?></span>
            </div>
            <?php endforeach; ?>
        </div>
    </section>
<?php endif; ?>