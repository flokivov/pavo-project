<?php
$title = get_field('buttons_section_title');
$repeater = get_field('buttons_section_repeater');
$id = get_field('buttons_section_id');
?>

<?php if($title || $repeater) : ?>
<section class="what-have" <?php if($id) : ?>id="<?php echo $id; ?>"<?php endif; ?> >
    <div class="container">
        <div class="row text-center">
            <?php if ($title) : ?><h2><?php echo $title; ?></h2><?php endif; ?>
            <?php if ($repeater) : ?>
                <div class="d-flex justify-content-center">
                    <?php foreach ($repeater as $row) :
                        $link = $row['buttons_section_repeater_link'];
                        if ($link) : ?><a class="btn-orange-b0" href="<?php echo esc_url($link['url']); ?>" role="button" aria-disabled="true"><?php echo esc_html($link['title']); ?></a><?php endif; ?>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>
<?php endif; ?>