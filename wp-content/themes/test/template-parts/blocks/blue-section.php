<?php
$title = get_field('blue_section_title');
$description = get_field('blue_section_description');
$image = get_field('image_blue_white_image');
$select = get_field('image_blue_white_position');
$id = get_field('blue_section_id');
?>

<?php if($select == 'Right'): ?>
<section class="image-blue-white image-blue" <?php if($id) : ?>id="<?php echo $id; ?>"<?php endif; ?> >
        <div class="row w-100 mx-0">
            <div class="col-lg-7 p-0">
                <div class="section-bg-blue container-right-section">
                    <?php if($title): ?><h5><?php echo $title; ?></h5><?php endif; ?>
                        <?php if($description): ?>  <div class="wysiwyg"><?php echo $description; ?></div> <?php endif; ?>
                </div> 
            </div>
            <div class="col-lg-5 p-0" style="background-color:#fff;<?php if ($image) : ?>background-image:url('<?php echo $image; ?>'); <?php endif; ?>">
            </div>

        </div>
    </section>
<?php else : ?>
    <section class="image-blue-white image-blue" <?php if($id) : ?>id="<?php echo $id; ?>"<?php endif; ?> >
        <div class="row w-100 mx-0">
        <div class="col-lg-5 p-0" style="background-color:#fff;<?php if ($image) : ?>background-image:url('<?php echo $image; ?>'); <?php endif; ?>">
            </div>
            <div class="col-lg-7 p-0">
                <div class="section-bg-blue container-right-section">
                    <?php if($title): ?><h5><?php echo $title; ?></h5><?php endif; ?>
                        <?php if($description): ?>  <div class="wysiwyg"><?php echo $description; ?></div> <?php endif; ?>
                </div> 
            </div>
            

        </div>
    </section>
<?php endif; ?>