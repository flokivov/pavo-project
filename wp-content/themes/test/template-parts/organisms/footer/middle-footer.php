<?php

if (!defined('ABSPATH')) exit;
$portfolioTitle = get_field('option_footer_portfolio_title', 'option');
$portfolioRepeater = get_field('option_footer_portfolio_repeater', 'option');
$formTitle = get_field('option_footer_form_title', 'option');
$phone = get_field('option_footer_form_phone', 'option');
$email = get_field('option_footer_form_email', 'option');
$location = get_field('option_footer_form_location', 'option');
$info = get_field('option_footer_form_info', 'option');
$copyright = get_field('option_footer_form_copyright', 'option');


?>
<div class="o-footer__middle">
    <div class="white-section"></div>
    <div class="container">
        <div class="portoflio" id="portfolio">
            <?php if ($portfolioTitle) : ?><h2><?php echo $portfolioTitle; ?></h2><?php endif; ?>
            <?php if ($portfolioRepeater) : ?>
                <div class="d-flex">
                    <?php foreach ($portfolioRepeater as $row) :
                        $rowLink = $row['option_footer_portfolio_repeater_url'];
                        $rowTitle = $row['option_footer_portfolio_repeater_title'];
                        $rowDescription = $row['option_footer_portfolio_repeater_description'];
                        $rowImage = $row['option_footer_portfolio_repeater_background'];
                    ?>
                        <a class="col-lg-4" href="<?php echo esc_url($rowLink); ?>" style="background-image:url('<?php echo esc_url($rowImage); ?>');">
                            <div class="">
                                <h5><?php echo $rowTitle; ?></h5>
                                <span><?php echo $rowDescription; ?></span>
                            </div>
                        </a>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>

        <div class="form" id="contact">
            <div class="row">
                <div class="col-lg-4">
                    <?php if ($formTitle) : ?><h2><?php echo $formTitle; ?></h2><?php endif; ?>
                    <?php if ($phone) : ?>
                        <div class="row">
                            <div class="col-lg-1">
                                <?php echo get_phone(); ?>
                            </div>
                            <div class="col-lg-10">
                                <a href="tel:<?php echo $phone; ?>"><?php echo $phone; ?></a>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if ($email) : ?>
                        <div class="row">
                            <div class="col-lg-1">
                                <?php echo get_mail(); ?>
                            </div>
                            <div class="col-lg-10">
                                <a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if ($location) : ?>
                        <div class="row">
                            <div class="col-lg-1">
                                <?php echo get_home(); ?>
                            </div>
                            <div class="col-lg-10">
                                <div><?php echo $location; ?></div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if ($info) : ?>
                        <div class="row">
                            <div class="col-lg-1">
                                <?php echo get_info(); ?>
                            </div>
                            <div class="col-lg-10">
                                <div><?php echo $info; ?></div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="col-lg-8">
                    <div class="row">
                        <?php echo do_shortcode('[contact-form-7 id="90" title="Contact form 1"]'); ?>
                    </div>
                </div>
            </div>
        </div>
        <?php if ($copyright) : ?>
            <div class="copyright">
                <div class="row">
                    <div class="col-lg-4">
                        <span><?php echo $copyright; ?></span>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>