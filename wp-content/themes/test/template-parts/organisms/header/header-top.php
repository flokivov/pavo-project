<?php

if (!defined('ABSPATH')) exit;

?>
<nav class="navbar navbar-dark navbar-expand-lg bg-body-tertiary">
  <div class="container">
  <?php the_custom_logo(); ?>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
      <?php wp_nav_menu([
                        'menu' => 'Main menu',
                        'menu_id' => 'bottom-menu',
                        'container' => false,
                        'container_class' => false,
                        'menu_class' => 'navbar-nav',
                        'add_li_class'  => 'nav-item',
                        'add_a_class'     => 'nav-link',
                    ]); ?>

                    <div class="language">
                        <?php echo get_file_language(); ?>
                        <label class="language-switcher">
              <input type="checkbox">
              <span class="slider round"></span>
              <span class="select-fr">EN</span>
              <span class="select-en">NL</span>
            </label>
                    </div>
    </div>
  </div>
</nav>