<?php

if (!defined('ABSPATH')) exit;


/**
 * Disable Font Size selection
 */
add_theme_support('disable-custom-font-sizes');
add_theme_support('disable-custom-colors');
add_theme_support('editor-color-palette');

/**
 * Custom Theme Colors
 */
add_theme_support('editor-color-palette', [
    [
        'name'  => 'White',
        'slug'  => 'white',
        'color'    => '#fff',
    ],
]);

/**
 * Custom font sizes
 */
add_theme_support(
    'editor-font-sizes',
    [
        [
            'name'      => __('Small', 'tech-task'),
            'shortName' => __('S', 'tech-task'),
            'size'      => 14,
            'slug'      => 'small'
        ],
    ]
);


/**
 * Whitelist of blocks
 */
add_filter('allowed_block_types_all', function ($allowed_block_types, $editor_context) {
    // allowed blocks by page/post type
    if (!empty($editor_context->post) && $editor_context->post->post_type === 'page') {
        /* return [
             'core/paragraph',
             'core/heading'
         ];*/
    }
    return [
        'core/paragraph',
        'core/video',
        'core/heading',
        'core/list',
        'core/block',
        'core/image',
        'core/gallery',
        'core/embed',
        'acf/spacer',
        'acf/first-sections',
        'acf/buttons-section',
        'acf/image-blue-white',
        'acf/icons-section',
        'acf/blue-section'
    ];
}, 99, 2);

/**
 * Wrap in container class
 */
add_filter('render_block', function ($block_content, $block) {
    $containerised_blocks = [];
    if (in_array($block['blockName'], $containerised_blocks)) {
        $content = '<div class="container">';
        $content .= $block_content;
        $content .= '</div>';
        return $content;
    }

    return $block_content;
}, 10, 2);

add_filter( 'acf/fields/wysiwyg/toolbars' , 'acf_toolbars'  );
function acf_toolbars( $toolbars )
{
    // Add a new toolbar called "Very Simple"
    // - this toolbar has only 1 row of buttons
    // formatselect,bold,italic,bullist,numlist,blockquote,alignleft,aligncenter,alignright,link,unlink,wp_more
    // spellchecker,fullscreen,wp_adv,strikethrough,hr,forecolor,pastetext,removeformat,charmap,outdent,indent,undo,redo,wp_help
    $toolbars['Very Simple' ] = array();
    $toolbars['Very Simple' ][1] = array('bold' , 'italic' , 'underline','link','unlink' );


    return $toolbars;
}