<?php

if (!defined('ABSPATH')) exit;

/**
 * Create Global Elements Menu Item
 */
if (function_exists('acf_add_options_page')) {

	# Site Globals (Parent)
	$site_globals = acf_add_options_page(array(
		'page_title' => 'Theme Settings',
		'menu_title' => 'Theme Settings',
		'icon_url' => 'dashicons-admin-generic',
		'redirect' => true
	));

	# JS Tracking
	$page_footer_settigs = acf_add_options_page(array(
		'page_title' => 'Footer settings',
		'menu_title' => 'Footer settings',
		'menu_slug' => 'footer',
		'parent_slug' => $site_globals['menu_slug'],
	));

	# JS Tracking
	$page_tracking_scripts = acf_add_options_page(array(
		'page_title' => 'Tracking Scripts',
		'menu_title' => 'Tracking Scripts',
		'menu_slug' => 'tracking',
		'parent_slug' => $site_globals['menu_slug'],
	));
}
