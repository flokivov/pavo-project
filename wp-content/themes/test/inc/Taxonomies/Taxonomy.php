<?php


namespace Inc\Taxonomies;


use Illuminate\Support\Str;
use Inc\Traits\Singleton;

class Taxonomy
{
    use Singleton;
    /**
     * The taxonomy name
     *
     * @var
     */
    protected string $name = '';

    /**
     * The taxonomy attributes
     *
     * @var array
     */
    protected array $attributes = [];

    protected array $post_types = [];

    public function setLabels($singular = 'Example', $plural = 'Examples'): array
    {
        $p_lower = strtolower($plural);
        $s_lower = strtolower($singular);
        return [
            'name' => $plural,
            'singular_name' => $singular,
            'menu_name' => $plural,
            'add_new_item' => "Add New $singular",
            'edit_item' => "Edit $singular",
            'view_item' => "View $singular",
            'view_items' => "View $plural",
            'search_items' => "Search $plural",
            'not_found' => "No $p_lower found",
            'not_found_in_trash' => "No $p_lower found in trash",
            'parent_item_colon' => "Parent $singular",
            'all_items' => "All $plural",
        ];
    }

    public function register(){
        register_taxonomy($this->name, $this->post_types, $this->attributes);
    }
}
