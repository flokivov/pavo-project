<?php


namespace Inc\Taxonomies;


class ExamplesCategories extends Taxonomy
{

    protected string $name = 'example_category';

    protected array $attributes = [
        'hierarchical' => true,
        'show_ui' => true,
        'public' => true,
        'show_admin_column' => true,
        'show_in_nav_menus' => true,
        'query_var' => true,
        'rewrite' => [
            'slug' => 'cat-example',
            'with_front' => false
        ]
    ];

    protected array $post_types = ['example'];

    public function __construct(array $args = [])
    {
        $this->attributes = array_merge(
            $this->attributes,
            [
                'labels' => $this->setLabels('Example Cat', 'Example Categories')
            ]
        );

        if ($args) {
            $this->attributes = array_merge($this->attributes, $args);
        }
    }
}
