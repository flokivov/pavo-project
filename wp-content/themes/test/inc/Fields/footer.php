<?php

namespace Inc\Fields;

use StoutLogic\AcfBuilder\FieldsBuilder;



$group = new FieldsBuilder(
    'footer',
    [
        'title' => __('Footer Settings', 'test'),
    ]
);

$group
    ->addTab('Portfolio')
    ->addText('option_footer_portfolio_title', [
        'label' => 'Title',
        'wrapper' => [
            'width' => '100%',
        ],
        'wpml_cf_preferences' => 2,
    ])
    ->addRepeater('option_footer_portfolio_repeater', [
        'label' => 'Repeater',
        'button_label' => 'Add block',
        'layout' => 'block',
        'max' => 3,
        'wpml_cf_preferences' => 3,
    ])
    ->addImage('option_footer_portfolio_repeater_background', [
        'label' => 'Background image',
        'wpml_cf_preferences' => 3,
        'return_format' => 'url',
        'wrapper' => [
            'width' => '100%',
        ],
    ])
    ->addText('option_footer_portfolio_repeater_title', [
        'label' => 'Title',
        'wrapper' => [
            'width' => '100%',
        ],
        'wpml_cf_preferences' => 2,
    ])
    ->addTextarea('option_footer_portfolio_repeater_description', [
        'label' => 'Description',
        'wrapper' => [
            'width' => '100%',
        ],
        'wpml_cf_preferences' => 2,
    ])
    ->addUrl('option_footer_portfolio_repeater_url', [
        'label' => 'Url',
        'wrapper' => [
            'width' => '100%',
        ],
        'wpml_cf_preferences' => 2,
    ])
    ->endRepeater()
    ->addTab('Form') 
    ->addText('option_footer_form_title', [
        'label' => 'Title',
        'wrapper' => [
            'width' => '100%',
        ],
        'wpml_cf_preferences' => 2,
    ])
    ->addText('option_footer_form_phone', [
        'label' => 'Phone number',
        'wrapper' => [
            'width' => '100%',
        ],
        'wpml_cf_preferences' => 2,
    ])
    ->addText('option_footer_form_email', [
        'label' => 'Email',
        'wrapper' => [
            'width' => '100%',
        ],
        'wpml_cf_preferences' => 2,
    ])
    ->addWysiwyg('option_footer_form_location', [
        'label' => 'Location',
        'wpml_cf_preferences' => 2,
        'rows' => 3,
        'new_lines' => 'br',
        'wrapper' => [
            'width' => '100%',
        ],
    ])
    ->addWysiwyg('option_footer_form_info', [
        'label' => 'Info',
        'wpml_cf_preferences' => 2,
        'rows' => 3,
        'new_lines' => 'br',
        'wrapper' => [
            'width' => '100%',
        ],
    ])
    ->addTab('Copyright') 
    ->addText('option_footer_form_copyright', [
        'label' => 'Copyright',
        'wrapper' => [
            'width' => '100%',
        ],
        'wpml_cf_preferences' => 2,
    ])
    ->setLocation('options_page', '==', 'footer');

return $group;
