<?php

namespace Inc\Blocks;

use StoutLogic\AcfBuilder\FieldsBuilder;

class FirstSections extends Block
{   
    protected string $name = 'first-sections';

    public function __construct()
    {

        parent::__construct([
            'name' => $this->name,
            'title' => __('First Sections'),
            'description' => __('Add'),
            'category' => 'formatting',
            'icon' => 'embed-photo',
            'mode' => 'edit',
            'supports' => [
                'anchor' => true,
                'align' => false,
                'mode' => true,
            ],
            'example' => [
                'attributes' => [
                    'mode' => 'preview',
                    'data' => [
                        'preview_image_hover_editor' => PREVIEWBLOCKIMGDIR . 'first-sections.png'
                    ]
                ],
            ],
       
        ]);

       
    }

    protected function registerFields(): array
    {
        $block = new FieldsBuilder($this->name);
        $block

            ->addMessage('First Sections','First Sections')
            ->addTab('first_sections_tab', [
                'label' =>  __('First section', 'test'),
            ])
            ->addText('first_sections_id', [
                'label' => 'Id',				
                'wpml_cf_preferences' => 2,
                'wrapper' => [
                    'width' => '50%',
                ],
            ])
            ->addText('first_sections_title', [
                'label' => 'Title',				
                'wpml_cf_preferences' => 2,
                'wrapper' => [
                    'width' => '50%',
                ],
            ])
            ->addLink('first_sections_link', [
                'label' => 'Button link',
                'wpml_cf_preferences' => 2,
                'wrapper' => [
                    'width' => '50%',
                ],
            ])
            ->addImage('first_sections_image', [
                'label' => 'Background Image',
                'wpml_cf_preferences' => 2,
				'return_format' => 'url',
                'required' => 1,
                'wrapper' => [
                    'width' => '50%',
                ],
            ])
            ->addWysiwyg('first_sections_description', [
                'label' => 'Description',
                'wpml_cf_preferences' => 2,
                'rows' => 3,
                'new_lines' => 'br',
                'wrapper' => [
                    'width' => '100%',
                ],
                'required' => 1,
            ])
            ->addTab('second_sections_tab', [
                'label' =>  __('Second section', 'test'),
            ])
            ->addText('second_sections_id', [
                'label' => 'Id',				
                'wpml_cf_preferences' => 2,
                'wrapper' => [
                    'width' => '50%',
                ],
            ])
            ->addText('second_sections_title', [
                'label' => 'Title',				
                'wpml_cf_preferences' => 2,
                'wrapper' => [
                    'width' => '50%',
                ],
            ])
            ->addLink('second_sections_first_link', [
                'label' => 'Button link',
                'wpml_cf_preferences' => 2,
                'wrapper' => [
                    'width' => '33%',
                ],
            ])
            ->addLink('second_sections_second_link', [
                'label' => 'Button link',
                'wpml_cf_preferences' => 2,
                'wrapper' => [
                    'width' => '33%',
                ],
            ])

            ->addImage('second_sections_image', [
                'label' => 'Background Image',
                'wpml_cf_preferences' => 2,
				'return_format' => 'url',
                'required' => 1,
                'wrapper' => [
                    'width' => '33%',
                ],
            ])
            ->addWysiwyg('second_sections_description', [
                'label' => 'Description',
                'wpml_cf_preferences' => 2,
                'rows' => 3,
                'new_lines' => 'br',
                'wrapper' => [
                    'width' => '100%',
                ],
                'required' => 1,
            ]);

        $block->setLocation('block', '==', sprintf('acf/%s', $this->name));
       

        return $block->build();
    }
}