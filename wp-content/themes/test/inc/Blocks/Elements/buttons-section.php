<?php

namespace Inc\Blocks;

use StoutLogic\AcfBuilder\FieldsBuilder;

class ButtonsSection extends Block
{   
    protected string $name = 'buttons-section';

    public function __construct()
    {

        parent::__construct([
            'name' => $this->name,
            'title' => __('Buttons Section'),
            'description' => __('Add Section'),
            'category' => 'formatting',
            'icon' => 'embed-photo',
            'mode' => 'edit',
            'supports' => [
                'anchor' => true,
                'align' => false,
                'mode' => true,
            ],
            'example' => [
                'attributes' => [
                    'mode' => 'preview',
                    'data' => [
                        'preview_image_hover_editor' => PREVIEWBLOCKIMGDIR . 'buttons-section.png'
                    ]
                ],
            ],
       
        ]);

       
    }

    protected function registerFields(): array
    {
        $block = new FieldsBuilder($this->name);
        $block

            ->addMessage('Buttons Section','Buttons Section')
            ->addText('buttons_section_id', [
                'label' => 'Id',				
                'wpml_cf_preferences' => 2,
                'wrapper' => [
                    'width' => '50%',
                ],
            ])
            ->addText('buttons_section_title', [
                'label' => 'Title',				
                'wpml_cf_preferences' => 2,
                'wrapper' => [
                    'width' => '50%',
                ],
            ])
            ->addRepeater('buttons_section_repeater', [
                'label' => 'Repeater',
                'button_label' => 'Add Button',
                'layout' => 'block',
                'wpml_cf_preferences' => 3,
            ])
            ->addLink('buttons_section_repeater_link', [
                'label' => 'Button',
                'wpml_cf_preferences' => 2,
                'wrapper' => [
                    'width' => '100%',
                ],
            ])
            ->endRepeater();

        $block->setLocation('block', '==', sprintf('acf/%s', $this->name));
       

        return $block->build();
    }
}