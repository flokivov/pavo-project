<?php

namespace Inc\Blocks;

use StoutLogic\AcfBuilder\FieldsBuilder;

class IconsSection extends Block
{   
    protected string $name = 'icons-section';

    public function __construct()
    {

        parent::__construct([
            'name' => $this->name,
            'title' => __('Icons Section'),
            'description' => __('Add Section'),
            'category' => 'formatting',
            'icon' => 'embed-photo',
            'mode' => 'edit',
            'supports' => [
                'anchor' => true,
                'align' => false,
                'mode' => true,
            ],
            'example' => [
                'attributes' => [
                    'mode' => 'preview',
                    'data' => [
                        'preview_image_hover_editor' => PREVIEWBLOCKIMGDIR . 'icons-section.png'
                    ]
                ],
            ],
       
        ]);

       
    }

    protected function registerFields(): array
    {
        $block = new FieldsBuilder($this->name);
        $block

            ->addMessage('Icons Section','Icons Section')
            ->addText('icons_section_id', [
                'label' => 'Id',				
                'wpml_cf_preferences' => 2,
                'wrapper' => [
                    'width' => '100%',
                ],
            ])
            ->addRepeater('icons_section_repeater', [
                'label' => 'Repeater',
                'button_label' => 'Add Button',
                'layout' => 'block',
                'max' => 4,
                'wpml_cf_preferences' => 3,
            ])
            ->addSelect('icons_section_select',[
                'label' => 'Icon',
                'wpml_cf_preferences' => 2,
                'choices' => [
                    'mechanism' => 'mechanism',
                    'ventilation' => 'ventilation',
                    'tools' => 'tools',
                    'propulsion' => 'propulsion'
                ],
                'wrapper' => [
                    'width' => '50%',
                ],
            ])
            ->addText('icons_section_text', [
                'label' => 'Title',				
                'wpml_cf_preferences' => 2,
                'wrapper' => [
                    'width' => '50%',
                ],
            ])
            ->endRepeater();

        $block->setLocation('block', '==', sprintf('acf/%s', $this->name));
       

        return $block->build();
    }
}