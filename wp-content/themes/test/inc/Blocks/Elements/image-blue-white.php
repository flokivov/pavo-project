<?php

namespace Inc\Blocks;

use StoutLogic\AcfBuilder\FieldsBuilder;

class ImageBlueWhite extends Block
{   
    protected string $name = 'image-blue-white';

    public function __construct()
    {

        parent::__construct([
            'name' => $this->name,
            'title' => __('Image Blue White'),
            'description' => __('Add'),
            'category' => 'formatting',
            'icon' => 'embed-photo',
            'mode' => 'edit',
            'supports' => [
                'anchor' => true,
                'align' => false,
                'mode' => true,
            ],
            'example' => [
                'attributes' => [
                    'mode' => 'preview',
                    'data' => [
                        'preview_image_hover_editor' => PREVIEWBLOCKIMGDIR . 'image-blue-white.png'
                    ]
                ],
            ],
       
        ]);

       
    }

    protected function registerFields(): array
    {
        $block = new FieldsBuilder($this->name);
        $block

            ->addMessage('Image Blue White block','Image Blue White block')
            ->addText('image_blue_white_id', [
                'label' => 'Id',				
                'wpml_cf_preferences' => 2,
                'wrapper' => [
                    'width' => '25%',
                ],
            ])
            ->addText('image_blue_white_nth', [
                'label' => 'Numbers',				
                'wpml_cf_preferences' => 2,
                'wrapper' => [
                    'width' => '25%',
                ],
            ])
            ->addText('image_blue_white_title', [
                'label' => 'Title',				
                'wpml_cf_preferences' => 2,
                'wrapper' => [
                    'width' => '50%',
                ],
                'required' => 1,
            ])
            ->addText('image_blue_white_subtitle', [
                'label' => 'SubTitle',				
                'wpml_cf_preferences' => 2,
                'wrapper' => [
                    'width' => '50%',
                ],
            ])
            ->addTrueFalse('image_blue_white_quote', [
                'label' => 'Quote?',
                'wpml_cf_preferences' => 2,
                'wrapper' => [
                    'width' => '25%',
                ],
            ])
            ->addLink('image_blue_white_link', [
                'label' => 'Button link',
                'wpml_cf_preferences' => 2,
                'wrapper' => [
                    'width' => '25%',
                ],
            ])
            ->addWysiwyg('image_blue_white_description', [
                'label' => 'Description',
                'wpml_cf_preferences' => 2,
                'rows' => 3,
                'new_lines' => 'br',
                'wrapper' => [
                    'width' => '100%',
                ],
                'required' => 1,
            ])
            
           
            ->addImage('image_blue_white_image', [
                'label' => 'Background Image',
                'wpml_cf_preferences' => 2,
				'return_format' => 'url',
                'required' => 1,
                'wrapper' => [
                    'width' => '50%',
                ],
            ])
            ->addSelect('image_blue_white_select',[
                'label' => 'Image position',
                'wpml_cf_preferences' => 2,
                'choices' => [
                    '1' => 'Left',
                    '2' => 'Right',
                ],
                'wrapper' => [
                    'width' => '50%',
                ],
            ]);

        $block->setLocation('block', '==', sprintf('acf/%s', $this->name));
       

        return $block->build();
    }
}