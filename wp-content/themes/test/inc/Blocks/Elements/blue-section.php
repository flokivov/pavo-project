<?php

namespace Inc\Blocks;

use StoutLogic\AcfBuilder\FieldsBuilder;

class BlueSection extends Block
{   
    protected string $name = 'blue-section';

    public function __construct()
    {

        parent::__construct([
            'name' => $this->name,
            'title' => __('Blue Section'),
            'description' => __('Add'),
            'category' => 'formatting',
            'icon' => 'embed-photo',
            'mode' => 'edit',
            'supports' => [
                'anchor' => true,
                'align' => false,
                'mode' => true,
            ],
            'example' => [
                'attributes' => [
                    'mode' => 'preview',
                    'data' => [
                        'preview_image_hover_editor' => PREVIEWBLOCKIMGDIR . 'blue-section.png'
                    ]
                ],
            ],
       
        ]);

       
    }

    protected function registerFields(): array
    {
        $block = new FieldsBuilder($this->name);
        $block

            ->addMessage('Blue section','Blue section')
            ->addText('blue_section_id', [
                'label' => 'Id',				
                'wpml_cf_preferences' => 2,
                'wrapper' => [
                    'width' => '50%',
                ],
            ])
            ->addText('blue_section_title', [
                'label' => 'Title',				
                'wpml_cf_preferences' => 2,
                'wrapper' => [
                    'width' => '50%',
                ],
            ])
            ->addWysiwyg('blue_section_description', [
                'label' => 'Description',
                'wpml_cf_preferences' => 2,
                'rows' => 3,
                'new_lines' => 'br',
                'wrapper' => [
                    'width' => '100%',
                ],
                'required' => 1,
            ])
            ->addImage('image_blue_white_image', [
                'label' => 'Background Image',
                'wpml_cf_preferences' => 2,
				'return_format' => 'url',
                'required' => 1,
                'wrapper' => [
                    'width' => '50%',
                ],
            ])
            ->addSelect('image_blue_white_position',[
                'label' => 'Image position',
                'wpml_cf_preferences' => 2,
                'choices' => [
                    'Left' => 'Left',
                    'Right' => 'Right',
                ],
                'wrapper' => [
                    'width' => '50%',
                ],
            ]);

        $block->setLocation('block', '==', sprintf('acf/%s', $this->name));
       

        return $block->build();
    }
}