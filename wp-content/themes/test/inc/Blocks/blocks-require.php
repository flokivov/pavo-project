<?php

use StoutLogic\AcfBuilder\FieldsBuilder;
use Inc\Blocks\FirstSections;
use Inc\Blocks\ButtonsSection;
use Inc\Blocks\ImageBlueWhite;
use Inc\Blocks\IconsSection;
use Inc\Blocks\BlueSection;


require_once 'elements/Block.php';
require_once 'elements/first-sections.php';
require_once 'elements/buttons-section.php';
require_once 'elements/image-blue-white.php';
require_once 'elements/icons-section.php';
require_once 'elements/blue-section.php';


/**
 * Initialize ACF Builder
 */

 add_action('init', function () {
    FirstSections::getInstance();
    ButtonsSection::getInstance();
    ImageBlueWhite::getInstance();
    IconsSection::getInstance();
    BlueSection::getInstance();
});