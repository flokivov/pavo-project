<?php

if (!defined('ABSPATH')) exit;

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function theme_widgets_init()
{

    $config = [
        'before_widget' => '<section class="widget %1$s %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h4>',
        'after_title'   => '</h4>'
    ];
    register_sidebar([
        'name'          => __('Footer', 'widget'),
        'id'            => 'footer-1'
    ] + $config);
}

//add_action('widgets_init', 'theme_widgets_init');