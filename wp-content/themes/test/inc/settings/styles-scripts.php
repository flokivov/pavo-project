<?php

if (!defined('ABSPATH')) exit;

/**
 * Styles and Scripts Loader
 */
class ScriptStyleLoader
{

  const JQUERY = 'jquery';

  const tt_JS = 'tt_js';

  const tt_STYLES = 'tt_styles';

  const tt_FONTS = 'tt_fonts';

  /**
   * Constructor
   */
  function __construct()
  {
    add_action('wp_enqueue_scripts', [$this, 'styles']);
    add_action('wp_enqueue_scripts', [$this, 'scripts']);
    add_filter('nav_menu_css_class', [$this, 'add_additional_class_on_li'], 1, 3);
    add_filter('nav_menu_link_attributes', [$this, 'add_additional_class_on_a'], 1, 3);
    add_filter('upload_mimes',  [$this, 'cc_mime_types']);
    add_filter('block_editor_rest_api_preload_paths', [$this, 'acf_filter_rest_api_preload_paths'], 10, 1);
  }

  /**
   * Styles Loader
   */
  function styles()
  {
    //Remove the Gutenberg Block Library CSS
    wp_dequeue_style('wp-block-library');
    wp_dequeue_style('wp-block-library-theme');


    //load fonts (If need more use | seprator )
    wp_enqueue_style('calibri-fonts', mix("/fonts/calibri/stylesheet.css"), '', false, 'screen');
    wp_enqueue_style('outfit-fonts', "//fonts.googleapis.com/css2?family=Outfit&display=swap", '', false, 'screen');
    wp_enqueue_style('custom-css', mix('/public/css/custom.css'), '', false);

    if (!is_admin()) {
      wp_register_style(self::tt_STYLES, mix('/public/css/app.css'), false, null);
      wp_enqueue_style(self::tt_STYLES);
    }
  }

  /**
   * Scripts Loader
   */
  function scripts()
  {
    if (!is_admin()) {
      wp_deregister_script(self::JQUERY);
      wp_register_script(self::JQUERY, mix('/public/js/app.js'), [], null, true);

      wp_enqueue_script(self::JQUERY);

      $script_vars = ['templateUrl' => get_stylesheet_directory_uri()];
      wp_localize_script(self::tt_JS, 'scriptVars', $script_vars);

      wp_enqueue_script('smooth-js', mix('/public/js/smoothScroll.js'), '', '', true);
    }
  }

  function add_additional_class_on_li($classes, $item, $args)
  {
    if (isset($args->add_li_class)) {
      $classes[] = $args->add_li_class;
    }
    return $classes;
  }

  function add_additional_class_on_a($classes, $item, $args)
  {
    if (isset($args->add_a_class)) {
      $classes['class'] = $args->add_a_class;
    }
    return $classes;
  }

  function cc_mime_types($mimes)
  {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
  }


  function acf_filter_rest_api_preload_paths($preload_paths)
  {
    if (!get_the_ID()) {
      return $preload_paths;
    }
    $remove_path = '/wp/v2/' . get_post_type() . 's/' . get_the_ID() . '?context=edit';
    $v1 =  array_filter(
      $preload_paths,
      function ($url) use ($remove_path) {
        return $url !== $remove_path;
      }
    );
    $remove_path = '/wp/v2/' . get_post_type() . 's/' . get_the_ID() . '/autosaves?context=edit';
    return array_filter(
      $v1,
      function ($url) use ($remove_path) {
        return $url !== $remove_path;
      }
    );
  }
}

new ScriptStyleLoader;
