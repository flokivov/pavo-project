<?php

namespace Inc\Traits;

trait Singleton
{
    public function __construct()
    {
    }

    final public static function getInstance()
    {
        static $instance = [];
        $called_class = get_called_class();

        if (!isset($instance[$called_class])) {
            $instance[$called_class] = new $called_class();
        }

        return $instance[$called_class];
    }
}